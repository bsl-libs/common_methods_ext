﻿// BSLLS:CommonModuleNameClient-off - без необходимости постфиксы не добавляем
// Сервис для обычных форм
//  

#Область ПрограммныйИнтерфейс

// Возвращает имя основного реквизита формы
// 
// Параметры: 
// 	Форма - Форма - Исследуемая форма
// 	
// Возвращаемое значение
// 	Строка|Неопределено
// 
Функция РеквизитОсновнойИмя(Форма) Экспорт
	
	Результат	= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		ДокументDOM = ПолучитьДокументDOMФормы(Форма);
		// BSLLS:Typo-off - корректно
		РазыменовательПИ = Новый РазыменовательПространствИменDOM(ДокументDOM);
		// BSLLS:Typo-on
		ИмяЭлемента = "/elem[1]/elem[1]/elem[2]/elem[1]/data[1]/text()";
		РезультатXPath = ДокументDOM.ВычислитьВыражениеXPath(ИмяЭлемента
		, ДокументDOM
		, РазыменовательПИ
		, ТипРезультатаDOMXPath.Строка);
		КлючОсновногоРеквизита = РезультатXPath.СтроковоеЗначение;
		
		ИмяЭлемента = "/elem[1]/elem[1]/elem[2]/elem[2]/elem/elem[1]/data[1]/text()";
		РезультатXPath = ДокументDOM.ВычислитьВыражениеXPath(ИмяЭлемента, ДокументDOM, РазыменовательПИ,
		ТипРезультатаDOMXPath.УпорядоченныйИтераторУзлов);
		Счетчик = 1;
		Пока Истина Цикл
			Узел = РезультатXPath.ПолучитьСледующий();
			Если Узел = Неопределено 
				ИЛИ Узел.ТекстовоеСодержимое = КлючОсновногоРеквизита Тогда
				Прервать;
			КонецЕсли;
			Счетчик = Счетчик + 1;
		КонецЦикла;
		
		Если Узел <> Неопределено Тогда
			СтрокаXPath		= "/elem[1]/elem[1]/elem[2]/elem[2]/elem[" + Счетчик + "]" 
			+ ПолучитьXPathИмениРеквизитаВОписанииРеквизита() + "/text()";
			РезультатXPath	= ДокументDOM.ВычислитьВыражениеXPath(СтрокаXPath
			, ДокументDOM
			, РазыменовательПИ
			, ТипРезультатаDOMXPath.Строка);
			Длина			= СтрДлина(РезультатXPath.СтроковоеЗначение);
			ОбрамлениеДлина	= 1;
			// BSLLS:MagicNumber-off - ложное срабатывание
			Результат		= Сред(РезультатXPath.СтроковоеЗначение
			, ОбрамлениеДлина + 1
			, Длина - 2 * ОбрамлениеДлина);
			// BSLLS:MagicNumber-on
		КонецЕсли;
	#Иначе
		ВызватьИсключение ом_ФормаОбычнаяТексты.СредаВыполненияОшибка();
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции // РеквизитОсновнойИмя

// Возвращает тип кнпоки обычной формы Действие
//
// Параметры:  
//
// Возвращаемое значение: 
// 	ТипКнопкиКоманднойПанели
//
Функция КнопкаТипДействие() Экспорт
	
	Результат	= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		Результат	= ТипКнопкиКоманднойПанели.Действие;
	#КонецЕсли 
		
	Возврат Результат;
	
КонецФункции // КнопкаТипДействие 

// Истина, если тип кнопки Действие
//
// Параметры: 
// 	Тип - ТипКнопкиКоманднойПанели - Исследуемый тип
//
// Возвращаемое значение: 
// 	Булево
//
Функция КнопкаТипДействиеЭто(Тип) Экспорт
	
	Возврат Тип = КнопкаТипДействие();
	
КонецФункции // КнопкаТипДействиеЭто 

// Возвращает тип кнпоки обычной формы Подменю
//
// Параметры:  
//
// Возвращаемое значение: 
// 	ТипКнопкиКоманднойПанели
//
Функция КнопкаТипПодменю() Экспорт
	
	Результат	= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		Результат	= ТипКнопкиКоманднойПанели.Подменю;
	#КонецЕсли 
		
	Возврат Результат;
	
КонецФункции // КнопкаТипПодменю

// Истина, если тип кнопки Подменю
//
// Параметры: 
// 	Тип - ТипКнопкиКоманднойПанели - Исследуемый тип
//
// Возвращаемое значение: 
// 	Булево
//
Функция КнопкаТипПодменюЭто(Тип) Экспорт
	
	Возврат Тип = КнопкаТипПодменю();
	
КонецФункции // КнопкаТипПодменюЭто 

// Возвращает тип кнпоки обычной формы Разделитель
//
// Параметры:  
//
// Возвращаемое значение: 
// 	ТипКнопкиКоманднойПанели
//
Функция КнопкаТипРазделитель() Экспорт
	
	Результат	= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		Результат	= ТипКнопкиКоманднойПанели.Разделитель;
	#КонецЕсли 
		
	Возврат Результат;
	
КонецФункции // КнопкаТипРазделитель

// Истина, если тип кнпоки Разделитель
//
// Параметры: 
// 	Тип - ТипКнопкиКоманднойПанели - Исследуемый тип
//
// Возвращаемое значение: 
// 	Булево
//
Функция КнопкаТипРазделительЭто(Тип) Экспорт
	
	Возврат Тип = КнопкаТипРазделитель();
	
КонецФункции // КнопкаТипРазделительЭто 

// Добавляет элемент на командную панель обычной формы
//
// Параметры:  
// 	Родитель - КоманднаяПанель - Родитель элемента
// 	ЭлементТип - ТипКнопкиКоманднойПанели - Тип элемента
// 	Имя - Строка - Имя элемента
// 	Действие - Строка - Имя метода
// 	Индекс - Число - Индекс элемента
//
// Возвращаемое значение: 
// 	КнопкаКоманднойПанели
//
Функция КоманднаяПанельЭлементДобавить(Родитель
	, ЭлементТип
	, Имя
	, Действие = Неопределено
	, Индекс = Неопределено) Экспорт
	
	Результат	= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		
		Коллекция	= Родитель.Кнопки;
		Номер		= ?(Индекс = Неопределено, Коллекция.Количество(), Индекс);
		
		Если КнопкаТипДействиеЭто(ЭлементТип) Тогда
			Результат	= Родитель.Кнопки.Вставить(Номер
			, Имя
			, ЭлементТип
			, Неопределено
			, Действие);
		Иначе
			Результат	= Родитель.Кнопки.Вставить(Номер
			, Имя
			, ЭлементТип);
		КонецЕсли;
	#Иначе
		ВызватьИсключение ом_ФормаОбычнаяТексты.СредаВыполненияОшибка();
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции // КоманднаяПанельЭлементДобавить  

// Возвращает имя типа даных формы
//
// Параметры:  
// 	Форма - Форма - Исследуемая форма
//
// Возвращаемое значение: 
// 	Строка - Справочник.ХХХ, Документ.ХХХ, ...
//
Функция ФормаДанныеТипИмя(Форма) Экспорт
	
	Результат			= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		РеквизитОсновнойИмя	= РеквизитОсновнойИмя(Форма);
		
		Если РеквизитОсновнойИмя <> Неопределено Тогда
			Результат	= ом_Значение.ТипИмяПолное(ом_Значение.СвойствоПолучить(Форма, РеквизитОсновнойИмя));
		Иначе
			ВызватьИсключение ом_ФормаОбычнаяТексты.РеквизитОсновнойИмяПолучитьОшибка();
		КонецЕсли;
	#Иначе
		ВызватьИсключение ом_ФормаОбычнаяТексты.СредаВыполненияОшибка();
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции // ФормаТипИмя 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ПолучитьДокументDOMФормы(Форма) 
	
	Результат	= Неопределено;
	
	#Если ТолстыйКлиентОбычноеПриложение ИЛИ ТолстыйКлиентУправляемоеПриложение Тогда
		СтрокаФормы		= ЗначениеВСтрокуВнутр(Форма);
		XMLСтрокаФормы	= ом_Строка.ВнутрВXMLТело(СтрокаФормы);
		ЧтениеXML		= Новый ЧтениеXML;
		ЧтениеXML.УстановитьСтроку(XMLСтрокаФормы);
		ПостроительDOM	= Новый ПостроительDOM;
		Результат		= ПостроительDOM.Прочитать(ЧтениеXML);
	#Иначе
		ВызватьИсключение ом_ФормаОбычнаяТексты.СредаВыполненияОшибка();
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции // ПолучитьДокументDOMФормы()

Функция ПолучитьXPathИмениРеквизитаВОписанииРеквизита()
	
	Возврат ?(ом_Версия.Сравнить(ом_СистемаИнформация.ПриложениеВерсия(), "8.1") <= 0
	, "/data[3]"
	, "/data[4]");
	
КонецФункции // ПолучитьXPathИмениРеквизитаВОписанииРеквизита()

#КонецОбласти

// BSLLS:CommonModuleNameClient-on
