﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЭтаФорма.Параметры.Свойство("ТестПараметр", ТестПараметр);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если ЭтаФорма.РежимОткрытияОкна = РежимОткрытияОкнаФормы.БлокироватьОкноВладельца Тогда
		// для теста необходимо подтверждение того, что 
		// форма была открыта, но поскольку мы отказывается от открытия,
		// то необходимо ручное оповещение
		ом_ДиалогКлиент.ОповещениеВыполнить(ЭтаФорма.ОписаниеОповещенияОЗакрытии
		, ТестПараметр);
		Отказ	= Истина;
	Иначе
		ВызватьИсключение "Неверный режим открытия формы!";
	КонецЕсли;
	
КонецПроцедуры
